﻿using UnityEngine;
using System.Collections;

public class floatsphtere : MonoBehaviour {
	public float translateZ;
	public float height;
	// Use this for initialization
	void Start () {
		translateZ = 0;
	}
	
	// Update is called once per frame
	void Update () {
		translateZ = Mathf.Cos(Time.time*1.5F) * 0.5F + 1;
		transform.position=new Vector3(transform.position.x, height + translateZ,transform.position.z);
	}
}
