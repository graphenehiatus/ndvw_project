﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnitySteer.Behaviors;

public class FollowAStar : MonoBehaviour {

	public Node nodeStart;
	public Node nodeFinish;
	public float threshold = 50;
	public ArrayList path = new ArrayList();
	private SteerForPoint steering;
	private Biped bi;
	private Animator animator;
	private Node target;
	public float speed;

	// Use this for initialization
	void Start () {
		steering = GetComponent<SteerForPoint>();
		bi = GetComponent<Biped>();
		animator = GetComponent<Animator>();
		if(path.Count == 0)
			path = AStar.FindPath(nodeStart,nodeFinish);
		target = (Node)path[0];
		steering.TargetPoint = target.position;
		path.RemoveAt(0);
		steering.enabled = true;
	}

	// Update is called once per frame
	void Update () {
		if(path.Count > 0 && Vector3.Distance (target.position, transform.position) < threshold) {
			target = (Node)path[0];
			path.RemoveAt(0);
			steering.TargetPoint = target.position;
		}
		animator.SetFloat("speed", bi.Speed);
	}
}
