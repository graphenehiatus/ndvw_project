using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Node : MonoBehaviour, IComparable
{
    #region Fields
    public float nodeTotalCost;         //Total cost so far for the node
    public float estimatedCost;         //Estimated cost from this node to the goal node
    public bool bObstacle;              //Does the node is an obstacle or not
	public List<Node> neighbours;                 
	public List<float> modifiers;
    public Vector3 position;            //Position of the node
	public Node parent;
	#endregion

    /// <summary>
    //Default Constructor
    /// </summary>
    public Node()
    {
        this.estimatedCost = 0.0f;
        this.nodeTotalCost = 1.0f;
        this.bObstacle = false;
		this.neighbours = new List<Node>();
		this.modifiers = new List<float>();
        this.parent = null;
    }

    /// <summary>
    //Constructor with adding position to the node creation
    /// </summary>
    public Node(Vector3 pos)
    {
        this.estimatedCost = 0.0f;
        this.nodeTotalCost = 1.0f;

        this.position = pos;
    }

	void Awake () 
		{
		this.position = transform.position;
		while (neighbours.Count>modifiers.Count)
			modifiers.Add((float)1.0);
	}


    /// <summary>
    // This CompareTo methods affect on Sort method
    // It applies when calling the Sort method from ArrayList
    // Compare using the estimated total cost between two nodes
    /// </summary>
    public int CompareTo(object obj)
    {
        Node node = (Node)obj;
        if (this.estimatedCost < node.estimatedCost)
            return -1;
        if (this.estimatedCost > node.estimatedCost)
            return 1;

        return 0;
    }
}


