﻿using UnityEngine;
using System.Collections;

public class texture : MonoBehaviour {
	public Renderer rend;
	public float scaleX;
	public float scaleY;
	void Start() {
		rend = GetComponent<Renderer>();
		scaleX = Mathf.Cos(Time.time) * 0.5F + 1;
		scaleY = Mathf.Sin(Time.time) * 0.5F + 1;
	}
	void Update() {
		scaleX = Mathf.Cos(Time.time) * 0.5F + 1;
		scaleY = Mathf.Sin(Time.time+ 3) * 0.5F + 1;
		rend.material.SetTextureScale("_Splat3", new Vector2(scaleX, scaleY));
		rend.material.SetTextureOffset("_Splat2", new Vector2(scaleY, scaleX));
	}
}