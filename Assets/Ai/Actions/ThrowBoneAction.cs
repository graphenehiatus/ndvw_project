using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class ThrowBoneAction : RAINAction
{

	private Guardian guardian;

	/// <summary>
    /// Damage is the helath reduction after the bone strikes the player
    /// </summary>
    public Expression BoneDamage = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		float fdamage = BoneDamage.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
		guardian.throwBone(AIUtils.getEnemyTransform(ai), fdamage);
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }

}