﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class InteractKey : MonoBehaviour {

    private GameObject door;
    private GameObject altar;
    private bool sceneChanged;
    private bool notSummoned;

    public GameObject boneMountain;
    public GameObject finalBoss;
    public int neededBones;

	// Use this for initialization
	void Start () {
        sceneChanged = false;
        door = GameObject.FindWithTag("door");
        altar = GameObject.FindWithTag("altar");
        DontDestroyOnLoad(transform.gameObject);
        notSummoned = true;
	}
	
	// Update is called once per frame
	void Update()
    {
        if (door == null || sceneChanged)
        {
            door = GameObject.FindWithTag("door");
            altar = GameObject.FindWithTag("altar");
            sceneChanged = false;
        }
        // Interact Key "E" to open doors and 
        if (Input.GetKeyDown("e"))
        {
            // Is there a door around?
            if (Vector3.Distance(transform.position, door.transform.position) < 40)
            {
                // Find orbiting spheres so they will follow
                GameObject[] spheres = GameObject.FindGameObjectsWithTag("hand");
                foreach (GameObject sphere in spheres)
                {
                    Behaviours sphereBeh = sphere.GetComponent<SphereController>().getController().beh;
                    if (sphereBeh == Behaviours.orbiting || sphereBeh == Behaviours.returning)
                    {
                        DontDestroyOnLoad(sphere);
                    }
                }
                // Change scene
                string currentSceneName = SceneManager.GetActiveScene().name;
                string newSceneName = (currentSceneName.Equals("Tutorial-start")) ? "Boss-scene" : "Tutorial-start";
                UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Assets/Tutorial/" + newSceneName + ".unity", LoadSceneMode.Single); 
                if(newSceneName.Equals("Tutorial-start")) this.transform.position += new Vector3(-10,0,0);
                sceneChanged = true;
            }
            else if (notSummoned && altar != null && Vector3.Distance(transform.position, altar.transform.position) < 40)
            {
                // Access to the bone counter
                Debug.Log(GameObject.FindWithTag("canvas").transform.GetChild(3).name);
                BoneDisplay boneDisplay = GameObject.FindWithTag("canvas").transform.GetChild(3).gameObject.GetComponent<BoneDisplay>();
                int bones = boneDisplay.GetValue();
                if (bones >= neededBones)
                {
                    notSummoned = false;
                    boneDisplay.Change(-neededBones);
                    GameObject bossSpawner = GameObject.Find("BossSpawner");
                    Transform bossTransform = bossSpawner.transform;
                    Transform bonesSpawner = GameObject.Find("BonesSpawner").transform;
                    Instantiate(boneMountain,bonesSpawner.transform.position + transform.up*4.0f,boneMountain.transform.rotation);
                    Instantiate(finalBoss,bossTransform);
                    BossSpawnerSound bossGrowl = bossSpawner.GetComponent<BossSpawnerSound>();
                    bossGrowl.PlaySound(); 
                }
            }
        }
	}
}
