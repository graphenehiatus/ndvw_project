﻿using UnityEngine;
using System.Collections;

public class navigation : MonoBehaviour {

	public float speed;
	public Transform target;
	public float threshold;
	public int rotation;
	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update ()
	{
		transform.Translate (speed * (transform.position.x - target.position.x) * Time.deltaTime,
			0,
			speed * (transform.position.z - target.position.z) * Time.deltaTime);

		if (rotation > 0) {
			transform.Rotate(0, 10, 0);
			rotation -= 10;
		}
			
	}

	public bool collision ()
	{
		return Vector3.Distance (target.position, transform.position) < threshold;
	}
}
