﻿using UnityEngine;
using System.Collections;
using UnitySteer.Behaviors;

[RequireComponent(typeof(navigation))]
public class seekRobot : MonoBehaviour {

	public navigation mov;
	public Animator animator;
	public seekRobot opponent;
	public bool chaser;
	public int refresh;

	// Use this for initialization
	void Start ()
	{
		animator = GetComponent<Animator> ();
		mov = GetComponent<navigation> ();
		if (!chaser) {
			refresh = -1;
		} else {
			mov.speed = 0;
			refresh = 60;
		}
			
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 robotCurrPos;
		Vector3 robotTargetPos = new Vector3 ();

		robotCurrPos = transform.position;

		if (refresh-- < 0 && chaser) {
			mov.speed = 0.2f;
			//animator.SetInteger ("speed", 5);
		}

		if (refresh >= 0) {
			mov.speed = 0;
			//animator.SetInteger ("speed", 0);
		}

		if (!chaser) {
			mov.speed = 0.16f;
			//animator.SetInteger ("speed", 5);
		}

		if (mov.collision () && refresh < 0 && chaser) {
			print ("CHANGING ROLES");
			opponent.chaser = true;
			opponent.refresh = 300;
			opponent.mov.rotation = 180;
			mov.rotation = 180;
			chaser = false;
		}
			
	}
}
