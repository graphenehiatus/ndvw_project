using System;

public enum Behaviours { wandering, returning, orbiting, picking, attacking };

public class Sphere {
	
	private FiniteStateMachine<Sphere> FSM;

	public Behaviours beh = Behaviours.wandering;
	public bool touched = false;
	public bool inRadius = false;
	public bool inTarget = false;
	public bool pick = false;
	public bool attacking = false;
	public int energy = 0;
	public int spheres = 0;

	public void ChangeState(FSMState<Sphere> e) {
		FSM.ChangeState(e);		
	}
	
	public void Awake() {
		Console.WriteLine ("Sphere awakes...");
		FSM = new FiniteStateMachine<Sphere>();
		FSM.Configure(this, SWander.Instance);
	}
 
	public void Update() {
		FSM.Update();
	}
}


