using System;


public sealed class SReturning :  FSMState<Sphere> {
	
	static readonly SReturning instance = new SReturning();
	public static SReturning Instance {
		get {
			return instance;
		}
	}
	static SReturning() { }
	private SReturning() { }
	
		
	public override void Enter (Sphere m) {
		if (m.beh != Behaviours.returning) {
			Console.WriteLine("Approaching for orbit");
			m.beh = Behaviours.returning;
		}
	}
	
	public override void Execute (Sphere m) {
		// Approach player

		if (m.inRadius)
			m.ChangeState(SOrbit.Instance);
	}
	
	public override void Exit(Sphere m) {
		Console.WriteLine("Got into range");
	}
}