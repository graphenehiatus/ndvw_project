﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SphereDisplay : MonoBehaviour {

	int _actual = 0;
    int _avail = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Text label = gameObject.GetComponent<Text>();
		label.text = "Spheres: " + _avail.ToString() + "/" + _actual.ToString();
	}

	public void Change (int val)
	{
		_actual += val;
	}

    public void ChangeAvail (int val)
    {
        _avail += val;
    }

	public int GetValue ()
	{
		return _actual;
	}
}
