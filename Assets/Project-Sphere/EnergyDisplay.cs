﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnergyDisplay : MonoBehaviour {

	int _max = 999;
	int _actual = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		_actual = _actual+1 > _max? _max : _actual+1;
		Text label = gameObject.GetComponent<Text>();
		label.text = "Energy: " + _actual.ToString() + "/" + _max.ToString();
	}

	public void Reduce (int val)
	{
		_actual -= val;
	}

	public int GetValue ()
	{
		return _actual;
	}
}
