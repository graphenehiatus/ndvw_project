﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

// TODO: displays could be integrated into a single class
// TODO: We could have some kind of SceneLoader class

public class LifeDisplay : MonoBehaviour {

	float _max = 100f;
	float _actual;

    public float restartDelay = 0.3f;
    float restartTimer;
    private bool died;

	// Use this for initialization
	void Start () {
		_actual = _max;
        died = false;
	}
	
	// Update is called once per frame
	void Update () {
		Text label = gameObject.GetComponent<Text>();
		label.text = "Health: " + _actual.ToString() + "/" + _max.ToString();
        if (died)
        {
            // If the user died start game over animation
            Animator anim = GameObject.FindWithTag("canvas").GetComponent<Animator>();
            anim.SetTrigger("GameOver");
            restartTimer += Time.deltaTime;
            Debug.Log("I DIED "+restartTimer+" SECONDS AGO");
            if (restartTimer >= restartDelay)
            {
                Debug.Log("Restarting Scene...");
                // Kill spheres
                GameObject[] spheres = GameObject.FindGameObjectsWithTag("hand");
                foreach (GameObject sphere in spheres)
                {
                    DestroyImmediate(sphere);
                }
                // Kill player
                DestroyImmediate(GameObject.Find("Player"));
                // Kill canvas
                DestroyImmediate(GameObject.FindWithTag("canvas"));
                // Restart scene
                UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Assets/Tutorial/Loader.unity",LoadSceneMode.Single); 
            }
        }
	}

	public void Heal (float val)
	{
		_actual += val;
		if (_actual + val > _max) {
			_actual = _max;
		}
		else {
			_actual += val;
		}
	}

	public void Reduce(float val)
    {
        _actual -= val;
        if (_actual <= 0 || died)
        {
            died = true;
            _actual = 0;
        }
	}

	public float GetValue ()
	{
		return _actual;
	}
}