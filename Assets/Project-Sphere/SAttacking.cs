using System;


public sealed class SAttacking :  FSMState<Sphere> {
	
	static readonly SAttacking instance = new SAttacking();
	public static SAttacking Instance {
		get {
			return instance;
		}
	}
	static SAttacking() { }
	private SAttacking() { }
	
		
	public override void Enter (Sphere m) {
		if (m.beh != Behaviours.attacking) {
			Console.WriteLine("Approaching to attack");
			m.beh = Behaviours.attacking;
		}
	}
	
	public override void Execute (Sphere m) {
		// Approach player

		if (m.inTarget)
			m.ChangeState(SReturning.Instance);
	}
	
	public override void Exit(Sphere m) {
		m.inTarget = false;
		Console.WriteLine("Returning to player");
	}
}