using System;


public sealed class SPicking :  FSMState<Sphere> {
	
	static readonly SPicking instance = new SPicking();
	public static SPicking Instance {
		get {
			return instance;
		}
	}
	static SPicking() { }
	private SPicking() { }
	
		
	public override void Enter (Sphere m) {
		if (m.beh != Behaviours.picking) {
			Console.WriteLine("Approaching to pickup");
			m.beh = Behaviours.picking;
		}
	}
	
	public override void Execute (Sphere m) {
		// Approach player

		if (m.inTarget)
			m.ChangeState(SReturning.Instance);
	}
	
	public override void Exit(Sphere m) {
		m.inTarget = false;
		Console.WriteLine("Returning to player");
	}
}