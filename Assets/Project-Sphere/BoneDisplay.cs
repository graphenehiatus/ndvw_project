﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoneDisplay : MonoBehaviour {

	int _actual = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Text label = gameObject.GetComponent<Text>();
		label.text = "Bone: " + _actual.ToString();
	}

	public void Change (int val)
	{
		_actual += val;
	}

	public int GetValue ()
	{
		return _actual;
	}
}
