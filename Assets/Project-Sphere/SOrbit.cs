using System;

public sealed class SOrbit :  FSMState<Sphere> {
	
	static readonly SOrbit instance = new SOrbit();
	public static SOrbit Instance {
		get {
			return instance;
		}
	}
	static SOrbit() { }
	private SOrbit() { }
		
	public override void Enter (Sphere m) {
		if (m.beh != Behaviours.orbiting) {
			Console.WriteLine ("Start to orbit");
			m.beh = Behaviours.orbiting;
		}
	}
	
	public override void Execute (Sphere m) {		
		// Orbit

		if (m.energy < 0)
			m.ChangeState(SWander.Instance);

		if (!m.inRadius)
			m.ChangeState(SReturning.Instance);

		if (m.pick)
			m.ChangeState(SPicking.Instance);

		if (m.attacking)
			m.ChangeState(SAttacking.Instance);
	}
	
	public override void Exit(Sphere m) {
		m.pick = false;
		Console.WriteLine ("Stop orbiting");
	}
}