using System;

public sealed class SWander :  FSMState<Sphere> {
	
	static readonly SWander instance = new SWander();
	public static SWander Instance {
		get {
			return instance;
		}
	}
	static SWander() { }
	private SWander() { }
	
		
	public override void Enter (Sphere m) {
		if (m.beh != Behaviours.wandering) {
			Console.WriteLine("Start to wander");
			m.beh = Behaviours.wandering;
		}
	}
	
	public override void Execute (Sphere m) {		
		// Wander

		if (m.touched)
			m.ChangeState(SReturning.Instance);
	}
	
	public override void Exit(Sphere m) {
		m.touched = false;
		Console.WriteLine("Stop wandering");
	}
}