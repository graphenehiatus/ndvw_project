﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public enum Colors { white, green, red };

public class SphereController : MonoBehaviour {

	Sphere _controller = new Sphere();
	Vector3 _origin;
	Vector3 _target;
	float _distance = 20;
	int _lastDirection = 1;
	float _height = 2;
	Colors _actual = Colors.white;
	EnergyDisplay _energyCounter;
	SphereDisplay _sphereDisplay;
	BoneDisplay _boneDisplay;
	Behaviours _last;
	Vector3 attack_target;

	public GameObject bone;
	private Transform player;
	public bool queue_for_attack;

    // Sphere speeds
    public float _speed = 25;
    public float _wanderSpeed = 10;
    public float _shootSpeed = 40;
    // Energy consumption values
    public int pickEnergyConsumption = 30;
    public int attackEnergyConsumption = 60;
    // Minimum distance for a sphere to cause damage
    public float minSphereDistance = 200.0f;
    // Damage the sphere causes to an enemy
    public float damage = 20.0f;
    public AudioClip sound;
    AudioSource audio;

	// Use this for initialization
	void Start () {
		_controller.Awake();
		_origin = transform.position;
		_target = _origin;
		_energyCounter = GameObject.Find("EnergyInterface").GetComponent<EnergyDisplay>();
		_sphereDisplay = GameObject.Find("SphereInterface").GetComponent<SphereDisplay>();
		_boneDisplay = GameObject.Find("BoneInterface").GetComponent<BoneDisplay>();
		_last = _controller.beh;
		queue_for_attack = false;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update()
    {
        _controller.Update();

        _controller.spheres = _sphereDisplay.GetValue();

        if (_last != _controller.beh && _last == Behaviours.wandering)
        {
            _sphereDisplay.Change(1);
            audio.PlayOneShot(sound, 1F);
        }

		if (_last != _controller.beh && _controller.beh == Behaviours.wandering)
			_sphereDisplay.Change (-1);

        if (_last != _controller.beh && _last == Behaviours.orbiting)
            _sphereDisplay.ChangeAvail (-1);

        if (_last != _controller.beh && _controller.beh == Behaviours.orbiting)
            _sphereDisplay.ChangeAvail (1);

		if (_last != _controller.beh && _controller.beh == Behaviours.attacking) {
			_energyCounter.Reduce (attackEnergyConsumption);
			attack_target = GetAttackTarget();
		}
			

		_last = _controller.beh;

		if (_actual != Colors.white && _controller.beh != Behaviours.returning && _controller.beh != Behaviours.picking) {
			ChangeColor (Color.white);
			_actual = Colors.white;
		}
			
		
		if (_controller.beh == Behaviours.wandering) {
			Wander ();
			CheckCollision ();
		} else if (_controller.beh == Behaviours.returning) {
			SeekPlayer ();
			CheckRange ();
		} else if (_controller.beh == Behaviours.orbiting) {
			Orbit ();
			CheckRange ();
			CheckKeys ();
		} else if (_controller.beh == Behaviours.picking) {
			if (bone == null) {
				bone = FindNearestBone ();
				if (bone == null) {
					_controller.inTarget = true;
					return;
				} else
					_energyCounter.Reduce (pickEnergyConsumption);
			}
			if (_actual != Colors.green) {
				ChangeColor (Color.green);
				_actual = Colors.green;
			}
			SeekTarget (bone.transform.position);
			CheckTarget (bone.transform.position);
		} else if (_controller.beh == Behaviours.attacking) {
			if (_actual != Colors.red) {
				ChangeColor (Color.red);
				_actual = Colors.red;
			}
			SeekTarget (attack_target);
			CheckTarget (attack_target);
		}
			
	}

	Vector3 GetAttackTarget ()
	{
		Vector3 direction = player.transform.forward;
		direction.Normalize();

		Vector3 targ = player.transform.position + direction*75;

		return targ;

	}

	GameObject FindNearestBone ()
	{
		GameObject[] gos = GameObject.FindGameObjectsWithTag ("grabbable");
		GameObject[] spheres = GameObject.FindGameObjectsWithTag("hand");

		List<GameObject> taken = new List<GameObject>();
		foreach (GameObject sph in spheres)
			taken.Add(sph.GetComponent<SphereController>().bone);

		GameObject output = null;
		float distance = float.MaxValue;
		float distTemp;
		foreach (GameObject bone in gos) {
			distTemp = Vector3.Distance (bone.transform.position, transform.position);
			if (distance > distTemp && !taken.Contains(bone)) {
				distance = distTemp;
				output = bone;
			}
		}

		return (output);
	}

	void ChangeColor (Color input)
	{
		Renderer renderer = GetComponentInChildren<Renderer>();
		Material mat = renderer.material;

		mat.SetColor ("_EmissionColor", input);
	}

	void SeekTarget (Vector3 target)
	{
		Vector3 dir = target - transform.position;
		dir.Normalize();
			

		transform.Translate (dir.x * Time.deltaTime * _shootSpeed,
				dir.y * Time.deltaTime * _shootSpeed,
				dir.z * Time.deltaTime * _shootSpeed,
				Space.World);
	}

	void Wander ()
	{
		if ( Vector3.Distance(transform.position, _target) < 5 )
			_target = _origin + new Vector3(UnityEngine.Random.value*20-10, 0, UnityEngine.Random.value*20-10);
		
		Vector3 mov = _target - transform.position;
		mov.Normalize();
		transform.Translate(mov.x * _wanderSpeed * Time.deltaTime, 0, mov.z * _wanderSpeed * Time.deltaTime, Space.World);
	}

	void CheckKeys ()
	{
		_controller.pick = Input.GetKeyDown (KeyCode.Mouse1);

		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			GameObject[] spheres = GameObject.FindGameObjectsWithTag ("hand");

			bool queue = false;
			foreach (GameObject sph in spheres)
				queue |= sph.GetComponent<SphereController> ().queue_for_attack;

			if (!queue) {
				queue_for_attack = true;
				_controller.attacking = true;
			}
		}

		_controller.energy = _energyCounter.GetValue();
	}

	void CheckCollision ()
	{
		Vector3 other = player.position;
		other.y = transform.position.y;

		if ( Vector3.Distance( transform.position, other ) < 10 )
			_controller.touched = true;
		else
			_controller.touched = false;
	}

	void CheckTarget (Vector3 other)
	{
        if (Vector3.Distance (transform.position, other) < 2) {
			queue_for_attack = false;
			_controller.inTarget = true;
			_controller.attacking = false;
		}
		else
			_controller.inTarget = false;
	}

	void CheckRange ()
	{
		
		if (Math.Abs (transform.position.x - player.position.x) + Math.Abs (transform.position.z - player.position.z) <= _distance) {
			if (bone != null) {
				Destroy (bone);
				_boneDisplay.Change(1);
				bone = null;
			}
			_controller.inRadius = true;
		}
		else
			_controller.inRadius = false;
	}

	void SeekPlayer ()
	{
		Vector3 dir = player.position - transform.position;
		//dir.y -= 10;
		dir.Normalize();
			

		transform.Translate (dir.x * Time.deltaTime * _speed,
				dir.y * Time.deltaTime * _speed,
				dir.z * Time.deltaTime * _speed,
				Space.World);

		if (bone != null)
			bone.transform.position = transform.position;
	}

	void Orbit ()
	{
		float difY = player.position.y - transform.position.y;
		//difY -= 10;

		if (difY > _height)
			_lastDirection = 1;
		if (difY < -_height)
			_lastDirection = -1;
		transform.RotateAround(player.position, Vector3.up, 10*Time.deltaTime*_wanderSpeed);

		transform.Translate (0,
				_lastDirection * Time.deltaTime * _wanderSpeed * 0.2f,
				0,
				Space.World);
	}

    public Sphere getController(){
        return _controller;
    }
}
