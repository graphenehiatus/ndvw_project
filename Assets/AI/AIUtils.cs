﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using RAIN.Core;
 

public class AIUtils
{

	// Returns the Guardian component attached to the owner of the behavior
	public static Guardian getGuardian(RAIN.Core.AI ai) {
		Guardian guard = ai.Body.GetComponent<Guardian>();
		if (guard == null) {
			throw new Exception("Owner of the AI object must be a guardian");
		}
		else {
			return guard;
		}
	}

	// Returns the FlameThrower component attached to the owner of the behavior
	public static FlameThrower getFlameThrower(RAIN.Core.AI ai) {
		FlameThrower ft = ai.Body.GetComponent<FlameThrower>();
		if (ft == null) {
			throw new Exception("Owner of the AI object must have a FlameThrower behavior");
		}
		else {
			return ft;
		}
	}

	// Returns enemy position
	public static Vector3 getGuardianPosition(RAIN.Core.AI ai) {
		return getGuardian(ai).transform.position;
	}

	// Returns the Guardia component attached to the owner of the behavior
	public static GameObject getEnemyObject(RAIN.Core.AI ai) {
		GameObject enemy = ai.WorkingMemory.GetItem<GameObject>("spotted");
		if (enemy == null) {
			throw new Exception("Enemy is not spotted: cannot retrieve");
		}
		else {
			return enemy;
		}
	}

	// Returns the Guardia component attached to the owner of the behavior
	public static RAIN.Entities.Aspects.RAINAspect getEnemyAspect(RAIN.Core.AI ai) {
		RAIN.Entities.Aspects.RAINAspect enemy = ai.WorkingMemory.GetItem<RAIN.Entities.Aspects.RAINAspect>("spottedPosition");
		if (enemy == null) {
			throw new Exception("Enemy is not spotted: cannot retrieve");
		}
		else {
			return enemy;
		}
	}

	// Returns enemy position
	public static Vector3 getEnemyPosition(RAIN.Core.AI ai) {
		return getEnemyAspect(ai).Position;
	}

	// Returns enemy transform
	public static Transform getEnemyTransform(RAIN.Core.AI ai) {
		return getEnemyAspect(ai).MountPoint;
	}

}


