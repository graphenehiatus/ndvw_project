using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class CheckNoShieldAction : RAINAction
{

	// Checks whether there is any non-protected guardian around

	private Guardian guardian;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		if (guardian.getUnprotectedMates().Count > 0) {
			return ActionResult.SUCCESS;
    	}
    	else {
			return ActionResult.FAILURE;
    	} 
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}