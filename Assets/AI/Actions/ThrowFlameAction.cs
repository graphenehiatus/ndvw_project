using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class ThrowFlameAction : RAINAction
{

	private FlameThrower thrower;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		thrower = AIUtils.getFlameThrower(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
    	thrower.Fire(AIUtils.getEnemyTransform(ai));
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}