using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class IsWeakAction : RAINAction
{

	// Sets variable 'isWeak' 

	/// <summary>
    /// Maximum life ratio to consider a player weak (e.g. 0.2)
    /// </summary>
    public Expression WeakRatio = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		float fRatio = WeakRatio.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
    	EnemyHealth health = ai.Body.gameObject.GetComponent<EnemyHealth>();
		ai.WorkingMemory.SetItem<bool>("isWeak", health.getRatio() <= fRatio);
		return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}