using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class CheckShieldTimeAction : RAINAction
{

	// Returns whether enough time has passes since the last protected action

	private Guardian guardian;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		if (guardian.canProtect()) {
			return ActionResult.SUCCESS;
    	}
    	else {
			return ActionResult.FAILURE;
    	} 
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}