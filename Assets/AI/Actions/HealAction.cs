using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class HealAction : RAINAction
{

	// This action heals the weakest guardian around

	private Guardian guardian;

	/// <summary>
    /// Damage is the helath reduction after one successful attack
    /// </summary>
    public Expression Healing = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
    	// Heal weakest one
		Guardian weakest = guardian.getWeakestMate();
		float fHealing = Healing.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
		guardian.healGuardian(weakest, fHealing);
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}