using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class DetectHarmedMates : RAINAction
{

	// This action returns whether there are harmed guardians around us

	private Guardian guardian;

	/// <summary>
    /// Minimum harm to consider a team mate harmed
    /// </summary>
    public Expression Harm = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		float harmf = Harm.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
		if (guardian.getHarmedMates(harmf).Count > 0) {
			return RAIN.Action.RAINAction.ActionResult.SUCCESS;
		}
		else {
			return RAIN.Action.RAINAction.ActionResult.FAILURE;
		}
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}