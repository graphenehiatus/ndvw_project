using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;


[RAINAction]
public class BossAttackAction : RAINAction
{

	// TODO: could be merged with AttackAction and make a single Attack behavior and action

	/// <summary>
    /// Damage is the helath reduction after one successful boss attack
    /// </summary>
    public Expression Damage = new Expression();

	/// <summary>
    /// Distance at which we inflict damage
    /// </summary>
    public Expression DamageDistance = new Expression();

	/// <summary>
    /// Target to damage
    /// </summary>
    public Expression Target = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		GameObject gTarget = Target.Evaluate<GameObject>(ai.DeltaTime, ai.WorkingMemory);
		float dist = DamageDistance.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);

		// Check if we are close enough. If so, damage player
		if (Vector3.Distance(ai.Body.transform.position, gTarget.transform.position) <= dist) {
			LifeDisplay playerLife = GameObject.Find("LifeInterface").GetComponent<LifeDisplay>();
			float fdamage = Damage.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
			playerLife.Reduce(fdamage);
			Debug.Log("Reducing player life to " + playerLife.GetValue());
       }

		return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}