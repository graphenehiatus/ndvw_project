using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class CheckTeamMatesAction : RAINAction
{

	// This action stores in isSquad whether we have other guardians around

	private Guardian guardian;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		ai.WorkingMemory.SetItem<bool>("isSquad", guardian.hasTeamMates());
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}