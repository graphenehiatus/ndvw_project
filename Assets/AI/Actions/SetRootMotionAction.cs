using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using RAIN.Motion;

[RAINAction]
public class SetRootMotionAction : RAINAction
{

	/// <summary>
    /// Whether to activate Root Motion or not. Root Motion to be enabled when we want to modify the transforms
    // of the object manually. Otherwise to let RAIN AI move the player
    /// </summary>
    public Expression Enabled = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		bool bEnable = Enabled.Evaluate<bool>(ai.DeltaTime, ai.WorkingMemory);
		((MecanimMotor)ai.Motor).UseRootMotion = bEnable;
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}