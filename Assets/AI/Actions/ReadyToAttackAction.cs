using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class ReadyToAttackAction : RAINAction
{

	/// <summary>
    /// Minimum time between attacks
    /// </summary>
    public Expression MinimumLapse = new Expression();

	/// <summary>
    /// Maximum time bewteen attacks
    /// </summary>
	public Expression MaximumLapse = new Expression();

	/// <summary>
    /// Whether we fix a certain amount of time. Set to negative for random
    /// </summary>
	public Expression FixedLapse = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
    	// Get next scheduled attack
		float next = ai.WorkingMemory.GetItem<float>("nextAttack");

		// Check enough time has passed
		if (next < -1) {
			// First time
			setNextAttack(ai);
			return ActionResult.FAILURE;
		}
		else {
			if (Time.time >= next) {
				setNextAttack(ai);
				return ActionResult.SUCCESS;
			}
			else {
				return ActionResult.FAILURE;
			}
		}
    }

    // Schedules the next attack randomly within a predefined window of time
    protected void setNextAttack(RAIN.Core.AI ai) {
		float fminLapse = MinimumLapse.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
		float fmaxLapse = MaximumLapse.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
		float fixedLapse = FixedLapse.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
		float lapse = fixedLapse >= 0 ? fixedLapse : Random.Range(fminLapse, fmaxLapse);
		ai.WorkingMemory.SetItem<float>("nextAttack", Time.time + lapse);
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}