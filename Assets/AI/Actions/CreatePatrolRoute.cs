using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class CreatePatrolRoute : RAINAction
{

	// This action creates a random patrol route around the initial point

	private Guardian guardian;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		guardian.generateInitialPatrol();
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}