using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using RAIN.Motion;

[RAINAction]
public class RotateAroundAction : RAINAction
{


	/// <summary>
    /// Target to rotate around
    /// </summary>
    public Expression Target = new Expression();

	/// <summary>
    /// Rotation speed around the player
    /// </summary>
    public Expression Speed = new Expression();

	/// <summary>
    /// Whether to rotate clockwise (true) or anti-clockwise
    /// </summary>
    public Expression Direction = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
    	// Get target, own body position and rotation speed and direction
		GameObject gTarget = Target.Evaluate<GameObject>(ai.DeltaTime, ai.WorkingMemory);
		float fRotation = Speed.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
		bool rotationDirection = Direction.Evaluate<bool>(ai.DeltaTime, ai.WorkingMemory);
		float modifier = rotationDirection ? 1 : -1;
		GameObject body = ai.Body.gameObject;

		// Start spinning around player
		((MecanimMotor)ai.Motor).UseRootMotion = true;
		body.transform.RotateAround(gTarget.transform.position, Vector3.up, Time.deltaTime * fRotation * modifier);

        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}