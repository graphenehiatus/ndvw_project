using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class LookToAction : RAINAction
{

	/// <summary>
    /// Target where to look at
    /// </summary>
    public Expression Target = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		GameObject gTarget = Target.Evaluate<GameObject>(ai.DeltaTime, ai.WorkingMemory);
		ai.Body.transform.LookAt(gTarget.transform);
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}