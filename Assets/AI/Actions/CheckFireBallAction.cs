using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class CheckFireBallAction : RAINAction
{

	private FlameThrower thrower;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		thrower = AIUtils.getFlameThrower(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		if (thrower.canLaunch()) {
			return ActionResult.SUCCESS;
    	}
    	else {
			return ActionResult.FAILURE;
    	} 
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}