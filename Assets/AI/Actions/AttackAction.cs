using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class AttackAction : RAINAction
{

	// Combat body attack

	private Guardian guardian;

	/// <summary>
    /// Damage is the helath reduction after one successful attack
    /// </summary>
    public Expression Damage = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		float fdamage = Damage.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);
		guardian.Attack(AIUtils.getEnemyPosition(ai), fdamage);
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }

}