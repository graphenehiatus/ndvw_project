using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class CheckPatrolRoute : RAINAction
{

	// This action is performed when the enemy has not spotted the player in the current iteration
	// Has two options: travel around last spotted position or travel in the initial area
	// The role of this action is to compute the routs for each option
	private Guardian guardian;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
    	guardian.checkPatrolState();
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}