using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class ProtectAction : RAINAction
{

	// This action build a shield randomly around one of the already non-protected guardians around

	private Guardian guardian;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
		guardian = AIUtils.getGuardian(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
    	IList<Guardian> nonShield = guardian.getUnprotectedMates();
		int randomIndex = Random.Range(0, nonShield.Count);
		guardian.protect(nonShield[randomIndex]);
		Debug.Log("Protecting " + nonShield[randomIndex].name);
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}