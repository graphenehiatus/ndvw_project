﻿using UnityEngine;
using System.Collections.Generic;
using RAIN.Navigation.Waypoints;

// TODO: We could restart patroling route after X amount of time
// TODO: try not to overlap positions with other guardians

public delegate void TeamMateDeathHandler(int id);

public class Guardian : MonoBehaviour {

	enum PatrolState { None, AroundEnemy, AroundOrigin };

	// Interval between attacks
	public float MinAttackInterval = 1.5f;

	// Interval between attacks
	public float MinHealInterval = 2.5f;

	// Interval between protects
	public float MinShieldInterval = 15f;

	// Initial patrolling position
	public Transform InitialPosition;

	// Number of points in routes
	public int RoutePoints = 5;

	// Range of random distances for patroling
	public float RandomRange = 500;

	// Time the enemy will be searching for the player after spotted and missed
	public float seekTime;

	// Prefab of the bone
	public GameObject bonePrefab;

	// Bone launch attributes
	private float minPower = 90.0f;
	private float maxPower = 110.0f;

	// Position where to launch bones
	public Transform launchPosition;

	// Healing prefab
	public GameObject healEffect;

	// Healing spot where to play healing effect
	public Transform healPoint;

	// Spot where to play defend effects
	public Transform ShieldPoint;

	// Spot where to play defend effects
	public GameObject ShieldEffect;

	// Shield duration
	public float ShieldSeconds = 8.0f;

	private PatrolState pState;
	private LifeDisplay playerLife;
	private float lastAttack;
	private float lastHeal;
	private float lastShield;
	private Vector3 lastSpottedPosition;
	private float lastSpottedTime;
	private WaypointRig waypoints;
	private EnemyHealth health;

	private float initShield;
	private bool hasShield;
	private GameObject shieldObj;

	// Team mate list. Indexed by the instance ID of the Guardian component
	private IDictionary<int, Guardian> team;

    // List of existing spheres
    private GameObject[] spheres;

	public event TeamMateDeathHandler OnOwnDeath; // Notify team mates that we died

	// Use this for initialization
	void Start () {
		// Get track of life interface
		playerLife = GameObject.Find("LifeInterface").GetComponent<LifeDisplay>();
		// Get track of waypoint rig
		waypoints = transform.parent.GetComponentInChildren<WaypointRig>();
		// Create random patrol route
		generateInitialPatrol();
		// Start patroling initial area
		pState = PatrolState.AroundOrigin;
		// Start last attack time
		lastAttack = Time.time;
		// Start last heal time
		lastHeal = Time.time;
		// Start last shield time
		lastShield = Time.time;
		// Set last spotted as null
		lastSpottedTime = -1;
		// Get health component and attach death listener
		health = GetComponent<EnemyHealth>();
		health.OnDeathEvent += OnGuardianDeath;
		// Set no shield
		hasShield = false;
		// Start empty team
		team = new Dictionary<int, Guardian>();
	}

	// Called when object is destructed
	void OnDestroy() {
		health.OnDeathEvent -= OnGuardianDeath;
	}

	// Update is called once per frame
	void Update()
    { 
        // Update shield state
        if (hasShield) {
            if (Time.time - initShield > ShieldSeconds) {
                finishShield();
            }
        }

        // Update sphere list
        spheres = GameObject.FindGameObjectsWithTag("hand");
        // Get my position
        Vector3 position = transform.position;
        // Check if any of the close spheres is attacking
        foreach (GameObject sphere in spheres)
        {
            // Check distance to sphere
            Vector3 diff = sphere.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            // Get the sphere state
            SphereController sphereState = sphere.GetComponent<SphereController>();
            // If sphere is closer than a threshold
            if (curDistance < sphereState.minSphereDistance)
            {
                // Get sphere controller
                Sphere sphereController = sphereState.getController();
                // If the sphere is attacking
                if (sphereController.attacking)
                {
                    Debug.Log("A sphere attacked guardian " + GetInstanceID());
                    // Tell the sphere it reached a target
                    sphereState.queue_for_attack = false;
                    sphereController.inTarget = true;
                    sphereController.attacking = false;
                    // Decrease enemy health
                    EnemyHealth myHealth = gameObject.GetComponent<EnemyHealth>();
                    if (hasShield) myHealth.damage(sphereState.damage/2.0f);
                    else myHealth.damage(sphereState.damage);
                }

            }
        }
		

	}

	// Destroy handler 
	public void OnGuardianDeath() {
		Debug.Log("Guardian " + GetInstanceID() + " died");
		if (OnOwnDeath != null) {
			OnOwnDeath(GetInstanceID()); // Send alerts to listeners
		}
		Destroy(transform.parent.gameObject);
	}

	// Sets last position the player has been seen
	public void setLastSeen(Vector3 position) {
		lastSpottedPosition = position;
		lastSpottedTime = Time.time;
	}

	// Returns True whether the player has been spotted less than 'seekTime' seconds ago
	public bool isTrackingPlayer() {
		return lastSpottedTime > 0f && Time.time <= (lastSpottedTime + seekTime);
	}

	// Returns last position the enemy has been seen
	public Vector3 getLastPosition(Vector3 position) {
		return lastSpottedPosition;
	}


	/*
	 * 	Teamwork functions
	 */

	// Team mate detected
	void OnTriggerEnter(Collider col) {
		if (GameUtils.isGuardian(col)) {
			Guardian otherGuardian = GameUtils.getGuardian(col);
			int id = otherGuardian.GetInstanceID();
			if (!team.ContainsKey(id)) {
				Debug.Log(name + ": Adding " + id.ToString() + " to team");
				team.Add(new KeyValuePair<int, Guardian>(id, GameUtils.getGuardian(col)));
				otherGuardian.OnOwnDeath += removeTeamMate;
			}
		}
	}

	// Team mate is gone
	void OnTriggerExit(Collider col) {
		if (GameUtils.isGuardian(col)) {
			int id = GameUtils.getGuardian(col).GetInstanceID();
			if (team.ContainsKey(id)) {
				removeTeamMate(id);
			}
		}
	}

	// Team mate is gone because of death
	void removeTeamMate(int id) {
		Debug.LogWarning("Mate " + id.ToString() + " removed from team");
		Guardian teamMate = team[id];
		teamMate.OnOwnDeath -= removeTeamMate;
		team.Remove(id);
	}

	public bool hasTeamMates() {
		return team.Keys.Count > 0;
	}

	public ICollection<Guardian> getTeamMates() {
		return team.Values;
	}


	/*
	 * 	Shield functions
	 */

	 public bool canProtect() {
	 	return Time.time - lastShield > MinShieldInterval;
	 }

	 public bool guardianHasShield() {
	 	return hasShield;
	 }


	 public void protect(Guardian g) {
		lastShield = Time.time;
		g.createShield();
	 }

	 protected void finishShield() {
	 	// Destroy particles and set to false
	 	Debug.Log("Destroying shield");
		Destroy(shieldObj);
		hasShield = false;
	 }

	 public void createShield() {
	 	// Instantiate object and set creation time
	 	initShield = Time.time;
	 	hasShield = true;
	 	shieldObj = (GameObject) Instantiate(ShieldEffect, ShieldPoint.position,
	 		ShieldPoint.rotation, ShieldPoint);
	 }

	// Returns team mate which has not already a shield
	public IList<Guardian> getUnprotectedMates() {
		List<Guardian> unproc = new List<Guardian>();
		foreach(Guardian g in getTeamMates()){
			if (!g.guardianHasShield()) {
				unproc.Add(g);
			}
		 }
		return unproc;
	}


	/*
	 * 	Healing functions
	 */

	public bool canHeal() {
		return Time.time > (lastHeal + MinHealInterval);
	}

	public float getHealthRatio() {
		return health.getRatio();
	}

	// Increases guardian health and plays health particle system
	public void heal(float increase) {
		health.heal(increase);
		Instantiate(healEffect, healPoint.transform.position, healPoint.transform.rotation, healPoint);
	}

	public float getHealth() {
		return health.getHealth();
	}

	// Heals another guardian
	public void healGuardian(Guardian g, float increase) {
		if (canHeal()) {
			Debug.Log("Healing guardian " + g.name);
			g.heal(increase);
			transform.LookAt(g.transform);
			lastHeal = Time.time;
		}
		else {
			Debug.Log("Can't heal, have to wait");
		}
	}

	// Returns team mate with lowest health (may be draws)
	public Guardian getWeakestMate() {
		Guardian result = null;
		float minHealth = float.MaxValue;
		foreach(Guardian g in getTeamMates()){
			if (g.getHealth() < minHealth) {
				minHealth = g.getHealth();
				result = g;
			}
		 }
		 return result;
	}

	// Returns team mates whose health is less than minRatio %
	public IList<Guardian> getHarmedMates(float minRatio) {
		List<Guardian> harmed = new List<Guardian>();
		foreach(Guardian g in getTeamMates()){
			if (g.getHealthRatio() < minRatio) {
				harmed.Add(g);
			}
		 }
		 return harmed;
	}


	/*
	 * 	Attack functions
	 */

	public bool canAttack() {
		return Time.time > (lastAttack + MinAttackInterval);
	}

	private void updateAttack() {
		lastAttack = Time.time;
	 }

	// Attacks the given object with the given power
	public void Attack(Vector3 enemyPos, float damage) {

		// Check if enough time has passed from previous attack
		if (canAttack()) {
			playerLife.Reduce(damage);
			Debug.Log("Inflicted " + damage.ToString() + " to enemy. Health now is " + playerLife.GetValue());
			updateAttack();
		}

	}


	// Throws a bone to the enemy. Uses same time interval as the body to body attack
	// This could be used as an abstract implementation of the Attack function, but
	// guardian role is already defined in its behavior tree
	public void throwBone(Transform enemyPos, float damage) {

		if (canAttack()) {
			// Instantiate object
			GameObject newBone = (GameObject) Instantiate(bonePrefab, launchPosition.position, launchPosition.rotation);
			Bone boneBehavior = newBone.GetComponent<Bone>();

			// Point to enemy and throw it
			boneBehavior.pointToEnemy(enemyPos);
			boneBehavior.setDamage(damage);
			float power = (UnityEngine.Random.value * (maxPower - minPower)) + minPower;
			boneBehavior.ThrowTo(enemyPos, power);

			updateAttack();
		}
	}


	/*
	 * 	Patrolling functions
	 */


	// This action is performed when the enemy has not spotted the player in the current iteration
	// Has two options: travel around last spotted position or travel in the initial area
	// The role of this action is to compute the routs for each option
	public void checkPatrolState() {
		if (pState.Equals(PatrolState.AroundOrigin) && isTrackingPlayer()) {
			// Change patrol route and state
			Debug.Log("Patroling around enemy");
			generateWaypoints(lastSpottedPosition, RoutePoints, RandomRange);
			pState = PatrolState.AroundEnemy;
		}
		else if (pState.Equals(PatrolState.AroundEnemy) && !isTrackingPlayer()) {
			// Change to origin patroling
			Debug.Log("Going back to original route");
			generateInitialPatrol();
			pState = PatrolState.AroundOrigin;
		}
	}


	// Generates route for patroling around initial position
	public void generateInitialPatrol() {
		generateWaypoints(InitialPosition.position, RoutePoints, RandomRange);
	}


	// Generates a random route around the given point using a range of possible points
	// Random range is the diameter of the circle of possible points for each axis
	// (Y excluded) around the given point
	public void generateWaypoints(Vector3 point, int numPoints, float randomRange) {
		// Compute radius
		float radius = randomRange/2f;
		// Clean waypoints
		waypoints.WaypointSet.RemoveAllWaypoints();
		// Create points in random fashion
		for (int i = 0; i < numPoints; ++i) {
			float newX = point.x + (Random.value * (radius)) - radius;
			float newZ = point.z + (Random.value * (radius)) - radius;
			Vector3 newPosition = new Vector3(newX, 0.0f, newZ);
			waypoints.WaypointSet.AddWaypoint(new Waypoint() { Position = newPosition });
		}
		// Connect points
		for (int i = 0; i < numPoints - 1; ++i) {
			waypoints.WaypointSet.AddConnection(i, i+1);
		}
		waypoints.WaypointSet.AddConnection(numPoints-1, 0); // Close loop
	}

}
