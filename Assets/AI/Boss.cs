﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour {

    // List of existing spheres
    private GameObject[] spheres;
    public event TeamMateDeathHandler OnOwnDeath; // Notify team mates that we died
    private EnemyHealth health;

	// Use this for initialization
	void Start () {
        // Get health component and attach death listener
        health = GetComponent<EnemyHealth>();
        health.OnDeathEvent += OnBossDeath;
	}
	
	// Update is called once per frame
	void Update () {
        // Update sphere list
        spheres = GameObject.FindGameObjectsWithTag("hand");
        // Get my position
        Vector3 position = transform.position;
        // Check if any of the close spheres is attacking
        foreach (GameObject sphere in spheres)
        {
            // Check distance to sphere
            Vector3 diff = sphere.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            // Get the sphere state
            SphereController sphereState = sphere.GetComponent<SphereController>();
            // If sphere is closer than a threshold
            if (curDistance < sphereState.minSphereDistance)
            {
                // Get sphere controller
                Sphere sphereController = sphereState.getController();
                // If the sphere is attacking
                if (sphereController.attacking)
                {
                    Debug.Log("A sphere attacked guardian " + GetInstanceID());
                    // Tell the sphere it reached a target
                    sphereState.queue_for_attack = false;
                    sphereController.inTarget = true;
                    sphereController.attacking = false;
                    // Decrease enemy health
                    EnemyHealth myHealth = gameObject.GetComponent<EnemyHealth>();
                    //if (hasShield) myHealth.damage(sphereState.damage/2.0f);
                    myHealth.damage(sphereState.damage);
                }

            }
        }
	}

    void OnDestroy() {
        health.OnDeathEvent -= OnBossDeath;
    }

    // Destroy handler 
    public void OnBossDeath() {
        Debug.Log("Boss died");
        if (OnOwnDeath != null) {
            OnOwnDeath(GetInstanceID()); // Send alerts to listeners
        }
        Animator anim = GameObject.FindWithTag("canvas").GetComponent<Animator>();
        anim.SetTrigger("Winning");
        Destroy(transform.parent.gameObject);
    }
}
