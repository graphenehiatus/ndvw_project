﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnitySteer.Behaviors;

public class FlockBehavior : MonoBehaviour {

	// These vectors show the range of positions the flock can move to
	public Vector3 limitsDown; 
	public Vector3 limitsUp;

	public float maxFlight; // Maximum seconds a flock will fly towards a point before changing direction
	public float minimumDistance;	// If flock is closer to destination

	private Vector3 nextPoint;	// Next point to head
	private float nextTime;	// Next time the flock will change destination

	private IList<FlockElem> children;

	// Use this for initialization
	void Start () {
		// Gather children
		List<GameObject> all = new List<GameObject> ();
		foreach (Transform child in transform) {
			all.Add(child.gameObject);
		}

		// Gather relative distances using first children as reference
		if (all.Count < 2) {
			throw new UnityException ("There must be at least 2 elements in the flock!");
		}

		// Build list of children according to reference element
		this.children = new List<FlockElem>();
		GameObject refElem = all[0];
		for (int i = 0; i < all.Count; ++i) {
			Debug.Log("Indentified butterfly " + i);
			GameObject currentObj = all[i];
			SteerForPoint currentSteer = currentObj.GetComponent<SteerForPoint>();
			Vector3 relDistance = refElem.transform.position - currentObj.transform.position;
			this.children.Add(new FlockElem(currentObj.transform, relDistance, currentSteer));
		}

		// Check flight input is correct
		if (this.maxFlight < 3) {
			throw new UnityException("Max flight value must be at least 3 seconds");
		}

		this.HeadNextPoint(); // Set to next objective
	}

	// Update is called once per frame
	// If enough time passed or too close to destination, move to next target
	void Update () {
		if (Time.time >= nextTime) {
			Debug.Log ("Moving to next position... ");
			this.HeadNextPoint();
		}
		if (this.TooClose()) {
			Debug.Log("Too close to destination. Changing route ...");
			this.HeadNextPoint();
		}
	}

	// Sets a new random direction for the flock
	private Vector3 GetNextPoint() {
		float newX = Random.Range(limitsDown.x, limitsUp.x);
		float newY = Random.Range(limitsDown.y, limitsUp.y);
		float newZ = Random.Range(limitsDown.z, limitsUp.z);
		return new Vector3(newX, newY, newZ);
	}

	// Whether reference element is too close to destination (avoid static flocks)
	private bool TooClose() {
		return Vector3.Distance (this.children[0].Position, this.nextPoint) < this.minimumDistance;
	}

	// Updates the next time the flock will change direction
	private void RefreshTimeLimit() {
		this.nextTime = Time.time + Random.Range(1, this.maxFlight);
	}

	// Sends the order to move flock towards new direction
	private void HeadNextPoint() {
		this.nextPoint = this.GetNextPoint();
		foreach (FlockElem fe in this.children) {
			fe.HeadToPoint(this.nextPoint);
		}
		this.RefreshTimeLimit ();
	}
}
