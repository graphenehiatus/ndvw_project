﻿using UnityEngine;
using System.Collections;

public class Bone : Throwable {

	private float magnitudeThreshold = 0.0f;

	void OnCollisionEnter(Collision other) {
		// Damage player only if bone has enough speed (launched)
		if (other.gameObject.tag == "Player" && rigid.velocity.magnitude > magnitudeThreshold) {
			Debug.Log("Player damaged by bone");
			HitPlayer(damage);
		}
	}

}
