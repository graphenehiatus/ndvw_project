﻿using UnityEngine;
using System.Collections;

public class AutoDestruct : MonoBehaviour {

	// Duration in seconds
	public float duration;
	private float initialTime;

	// Use this for initialization
	void Start () {	
		initialTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - initialTime > duration) {
			DestroyObject(transform.gameObject);
		}
	}

}
