﻿using UnityEngine;
using System.Collections;

public class Flame : Throwable {

	void OnCollisionEnter(Collision other) {


		Debug.Log(transform.gameObject);
		if (other.gameObject.tag == "Player") {
			Debug.Log("Player damaged by fire");
			HitPlayer(damage);
		}

		// Destroy at any collision
		Debug.Log(transform.gameObject);
		Destroy(transform.gameObject);
	}

}
