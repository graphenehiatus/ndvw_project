﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

	public AudioClip audioClip;

	// Use this for initialization
	void Start () {
		GameObject obj = GameObject.Find("PlayerBackgroundMusic");
		if (obj != null) {
			AudioSource aSource = obj.GetComponent<AudioSource>();
			aSource.clip = audioClip;
            aSource.volume = 0.2f;
            aSource.loop = true;
			aSource.Play();
		}
		else {
			Debug.LogError("Could not find music player");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
