﻿using UnityEngine;
using System.Collections;

public class FlameThrower : MonoBehaviour {

	public float power = 5f;
	public float ThrowInterval=2; // Time to wait to throw next fire ball
	public GameObject FireBall;
	public Transform LaunchPosition;
	public float FlameDamage = 10f;

	private float lastLaunched;

	// Use this for initialization
	void Start () {
		lastLaunched = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Returns whether enough time passed from previous launch
	public bool canLaunch() {
		return Time.time > lastLaunched + ThrowInterval;
	}

	// Sends a ball of fire to the target position
	public void Fire(Transform target) {
		GameObject inst = (GameObject) Instantiate(FireBall, LaunchPosition.position, LaunchPosition.rotation);
		Flame fire = inst.GetComponent<Flame>();
		// Point fire and shoot to enemy
		fire.pointToEnemy(target);
		fire.setDamage(FlameDamage);
		fire.ThrowTo(target, power);
		// Update time
		lastLaunched = Time.time;
	}

}
