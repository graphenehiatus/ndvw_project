﻿using UnityEngine;
using System.Collections;

public class AutoHealer : MonoBehaviour {

	// Class that automatically adds health to the player if the object that
	// has the behavior remains still

	private float lastChecked;
	private Vector3 lastPosition;

	private LifeDisplay playerLife;
	public float healAmount = 1.0f; // Healing after each still interval
	public float maxDistance = 0.1f; // Max distance to move object to consider no movement
	public float timeInvertal = 2.0f;

	// Use this for initialization
	void Start () {
		// Get track of life interface
		playerLife = GameObject.Find("LifeInterface").GetComponent<LifeDisplay>();
		lastChecked = Time.time;
		lastPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > lastChecked + timeInvertal) {
			Debug.Log(Time.time + ": healing player");
			// Check position displacement. If no movement, heal
			float displaced = Vector3.Distance(transform.position, lastPosition);
			if (displaced < maxDistance && playerLife.GetValue() != 0) {
				playerLife.Heal(healAmount);
			}
			// Update last known position and time
			lastPosition = transform.position;
			lastChecked = Time.time;
		}
	}
}
