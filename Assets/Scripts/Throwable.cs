﻿using UnityEngine;
using System.Collections;

public abstract class Throwable : MonoBehaviour {

	protected Rigidbody rigid;
	protected float damage;

	// Use this for initialization
	void Awake () {
		rigid = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () { }

	public void setDamage(float d) {
		damage = d;
	}

	public void ThrowTo(Transform dest, float power) {
		rigid.AddForce(forceToDest(dest, power));
	}

	public void pointToEnemy(Transform t) {
		transform.LookAt(t);
	}

	protected Vector3 forceToDest(Transform dest, float power) {
		Vector3 off = dest.position - transform.position;
		return (off) * power;
	}

	// Hurts the player. To call when hit detected
	protected void HitPlayer(float damage) {
		LifeDisplay playerLife = GameObject.Find("LifeInterface").GetComponent<LifeDisplay>();
		playerLife.Reduce(damage);
	}
}
