﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawnerSound : MonoBehaviour {

    public AudioClip sound;
    AudioSource audio;
    
    void Start() {
        audio = GetComponent<AudioSource>();
    }
    
    public void PlaySound() {
        audio.PlayOneShot(sound, 1F);
    }
}
