﻿using System;
using UnityEngine;

public class GameUtils
{

	// Returns the player GameObject
	public static GameObject getPlayer() {
		GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");
		if (objs.Length == 0) {
			throw new Exception("No player found!");
		}
		if (objs.Length > 1) {
			Debug.LogWarning("More than one player found. Using first one");
		}
		return objs[0];
	}

	// Given a collision, returns the guardian component of the other object
	public static Guardian getGuardian(Collider other) {
		Guardian g = other.gameObject.GetComponent<Guardian>();
		if (g == null) {
			throw new Exception("Guardian component not found in collision");
		}
		return g;
	}

	// Whether the collided object is a guardian or not
	public static bool isGuardian(Collider other) {
		return other.gameObject.GetComponent<Guardian>() != null;
	}

}

