﻿using UnityEngine;
using System.Collections;

public delegate void DeathEventHandler();

public class EnemyHealth : MonoBehaviour {

	private GameObject player;
	private GameObject healthInstance;
	private Renderer lifeRenderer;

	private float health;
	private Vector3 initialScale;

	// Colors for high, medium and low level life
	public Material HighColor;
	public Material MediumColor;
	public Material LowColor;

	// Object to use as life bar
	public GameObject LifeBar;
	public float VerticalOffset;
	public float InitialHealth;

	public event DeathEventHandler OnDeathEvent;

	// Use this for initialization
	void Start () {
		// Reference to player
		player = GameUtils.getPlayer();
		// Instantiate lifebar
		healthInstance = instantiate();
		// Get renderer to change bar color
		lifeRenderer = healthInstance.GetComponent<Renderer>();
		// Start health with max value
		health = InitialHealth;
		// Get initial scale
		initialScale = healthInstance.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		// Update size
		updateBar();
		// Always point to the user direction
		pointToPlayer();
		// Change color according to current life
		setBarColor();
		// Check if died
		if (health <= 0) {
			OnDeathEvent();
		}
	}

	// Generate lifebar object above character
	private GameObject instantiate() {
		Vector3 parentPosition = transform.position;
		Vector3 barPosition = new Vector3(transform.position.x, transform.position.y + VerticalOffset, transform.position.z);
		GameObject newObject = (GameObject) Instantiate(LifeBar, barPosition, transform.rotation);
		newObject.transform.parent = transform;
		return newObject;
	}

	// Sets lifebar color according to health
	private void setBarColor() {
		float lifeRatio = health / InitialHealth;
		Material[] materials = lifeRenderer.materials;
		if (lifeRatio < 0.20) {
			materials[0] = LowColor;
		}
		else if (lifeRatio < 0.60) {
			materials[0] = MediumColor;
		}
		else {
			materials[0] = HighColor;
		}
		lifeRenderer.materials = materials;
	}

	// Make the lifebar point to player for readability
	protected void pointToPlayer() {
		// Always point to the user direction
		healthInstance.transform.LookAt(player.transform);
	}

	// Decrease health and clip to 0
	public void damage(float amount) {
		health -= amount;
		if (health < 0) {
			health = 0;
		}
	}

	// Increase health and clip to maximum
	public void heal(float amount) {
		health += amount;
		if (health > InitialHealth) {
			health = InitialHealth;
		}
	}

	public float getRatio() {
		return health / InitialHealth;
	}

	public float getHealth() {
		return health;
	}

	// Updates bar according to life
	protected void updateBar() {
		float ratio = health / InitialHealth;
		Vector3 currentScale = healthInstance.transform.localScale;
		healthInstance.transform.localScale = new Vector3(initialScale.x * ratio, currentScale.y, currentScale.z);
	}

}
