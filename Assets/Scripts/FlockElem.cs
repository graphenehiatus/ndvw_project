﻿using UnityEngine;
using System;
using System.Collections;
using UnitySteer.Behaviors;


public class FlockElem
{
	private SteerForPoint steer;
	private Transform _transform;
	public Vector3 Position
	{
		get { return this._transform.position; }
	}

	public FlockElem(Transform pos, Vector3 offset, SteerForPoint sfp) {
		this._transform = pos;
		this.Distance = offset;
		this.steer = sfp;
	}

	private Vector3 _distance;
	public Vector3 Distance
	{
		get { return this._distance; }
		set { this._distance = value; }

	}

	// Moves to point according to offset of the flock element
	public void HeadToPoint(Vector3 point) {
		this.steer.TargetPoint = point + this.Distance;
	}

}

