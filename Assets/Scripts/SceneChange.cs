﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChange : MonoBehaviour {

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(transform.gameObject);
        SceneManager.LoadSceneAsync("Assets/Tutorial/Tutorial-start.unity", LoadSceneMode.Single); 
	}
	
	// Update is called once per frame
	void Update()
    {
        // When the scen is loaded fade the loading screen
        string currentSceneName = SceneManager.GetActiveScene().name;
        if (currentSceneName.Equals("Tutorial-start"))
        {
            Animator anim = GameObject.FindWithTag("canvas").GetComponent<Animator>();
            anim.SetTrigger("GameStarts");
            Destroy(GetComponent<SceneChange>());
        }
	}
}
